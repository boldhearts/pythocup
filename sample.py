from pythocup import Team, Match, FollowBall

def main():

    team_a = Team()
    team_a.put(FollowBall, (100, 250))

    team_b = Team()
    # team_b.put(FollowBall, (100, 250))

    match = Match()
    match.play(team_a, team_b)
    print(match.get_score())

if __name__ == "__main__":
    main()
