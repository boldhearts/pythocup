# Python Cup

This is based on Daniel Polani's [PythoCup](https://gitlab.com/boldhearts/pythocup-fltk).

PythoCup is a python-based simple simulator to study several aspects of the 2D RoboCup scenario without the hassle of understanding the detailed synchronization and command model of the standard 2D simulator. It simplifies many aspects, does not include turns, currently does not implement noise, outs (field boundaries) and many other features, and has been developed on-the-fly for the purpose of an quick-access to 2D RoboCup scenarios.

## Prerequirements
- Python 3 (installation [described below](#python-version-3))
- Pygame 1.9.3

## Install

You'll need python 3.5.3 or later. You can find out your python version with the command `python --version` or `python3 --version`.

### Clone this code

Create a folder named, e.g., `PythoCup` and clone the code:

    git clone git@gitlab.com:boldhearts/pythocup.git

### Create a virtual environment

Python's [virtual environments](https://docs.python.org/3/library/venv.html) come in handy for encapsulating dependencies from global installed packages. The following command uses the `venv` module and creates a folder named `pyenv-pythocup` with the environment:

	python3 -m venv pyenv-pythocup

You will need to source the environment in the terminal you're working in:

	source pyenv-pycup/bin/activate

If successful, the prompt of the terminal will have a prefix with the given name of the virtual environment (i.e. pyenv-pythocup). You can now install all requirments with the following command:

	pip3 install -r requirements.txt

Test if Pygame is working with running:

	python3 -m pygame.examples.aliens

## Python version 3

If your system doesn't have Python's version 3, you can install it the following way:

### Ubuntu/Linux

On older systems, you may have to install python version 3 like this:

```
sudo apt-get install python3 python3-pip
pip3 install pygame
```

### Windows
1. Download Python 3.5.3
  - https://www.python.org/downloads/
2. Install Python 3.5.3
  - On Windows make sure to add Python to PATH
3. Open the `Command Prompt` or `Terminal`
  1. Check the python version `python --version`
  2. `python -m pip install wheel`
4. Download .whl file
  - https://pypi.python.org/pypi/Pygame/1.9.3
  - 32 byte = pygame-1.9.3-cp35-cp35m-win32.whl
  - 64 byte = pygame-1.9.3-cp35-cp35m-win_amd64.whl 

### Mac
1. Download Python 3.5.3
  - https://www.python.org/downloads/
2. Install Python 3.5.3
  - On Windows make sure to add Python to PATH
3. `Terminal`
  1. Check the python version `python3 --version`
  2. `python3 -m pip install pygame`

# Further Information
You can read about used [classes](https://gitlab.com/scheunemann/pythocup/wikis/Useful-Classes) in the wiki. There is also a set of [exercises](https://gitlab.com/scheunemann/pythocup/wikis/Exercises) to begin with.

# Contribution
You're welcome to contribute. Please have a look at [Gitlab's workflow](https://docs.gitlab.com/ee/workflow/forking_workflow.html) first. It is helpful if each commit addresses a specific feature before requesting a merge. You can achieve this with, e.g., squashing the commits of your working copy.