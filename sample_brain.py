from pythocup import Team, Match, Brain, FollowBall
from pygame.math import Vector2

class GoToBall(Brain):

    def action(self):
        move = self.ball.pos - self.pos
        return move, Vector2(0,0)


def main():

    team_a = Team()
    team_a.put(GoToBall, (100, 250))

    team_b = Team()
    # team_b.put(FollowBall, (100, 250))

    match = Match()
    match.play(team_a, team_b)
    print(match.get_score())

if __name__ == "__main__":
    main()
