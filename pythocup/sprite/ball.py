import pygame
from .. import config
from .round_sprite import RoundSprite

class Ball(RoundSprite):

    def __init__(self, pos):
        super().__init__(20, config.BALL)
        self.start = pos
        self.reset()

    def setup(self, *teams):
        self.players = []
        for team in teams:
            for player in team.sprites():
                self.players.append(player)

    def update(self):
        self.move_with_collision(config.BALL_SPEED, self.force)
        self.force = pygame.math.Vector2()

    def reset(self):
        self.rect.center = self.start
        self.force = pygame.math.Vector2()

    def kick(self, vector):
        self.force += vector

    def collide(self):
        for player in self.players:
            if(pygame.sprite.collide_circle(self, player)):
                return True
        return False
