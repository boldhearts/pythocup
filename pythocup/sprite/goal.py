import pygame
from .. import config
from ..model import Side

class Goal(pygame.sprite.Sprite):

    def __init__(self, side):
        super().__init__()
        self.side = side
        self.image = pygame.image.load(config.GOAL).convert_alpha()
        self.rect = self.image.get_rect()

        if side == Side.LEFT:
            self.rect.midright = [40, 250]
        else:
            self.rect.midleft = [760, 250]

    def setup(self, ball, score):
        self.ball = ball
        self.score = score

    def update(self):
        if self.rect.contains(self.ball.rect()):
            self.ball.reset()
            self.score.increase()
