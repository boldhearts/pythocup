import pygame
from ..model import Side

pygame.font.init()

FONT = pygame.font.Font(None, 40)

class Score(pygame.sprite.Sprite):

    def __init__(self, side):
        super().__init__()
        self.side = side
        self.reset()

    def update(self):
        text = "{:02d}".format(self.score)
        self.image = FONT.render(text, 0, (0,0,0), (200,200,200))
        self.rect = self.image.get_rect()
        if self.side == Side.LEFT:
            self.rect.topright = (395,25)
        else:
            self.rect.topleft = (405,25)

    def increase(self):
        self.score += 1

    def reset(self):
        self.score = 0
