from abc import ABC, abstractmethod
import pygame

class RoundSprite(ABC, pygame.sprite.Sprite):

    def __init__(self, diameter, colour):
        super().__init__()
        self.image = pygame.Surface((diameter, diameter)).convert_alpha()
        self.rect = self.image.get_rect()
        self.image.fill((0, 0, 0, 0))
        self.radius = int(diameter/2)
        pygame.draw.circle(self.image, colour, self.rect.center , self.radius)

    def round_vector2(self, vect):
        x = round(vect[0])
        y = round(vect[1])
        return pygame.math.Vector2(x,y)

    @abstractmethod
    def collide(self):
        raise NotImplementedError('collide() has not been setup')

    def move_with_collision(self, speed, move_force ):
        #Set max_distance
        move_force = self.round_vector2(move_force)
        if move_force.length() > speed:
            max_distance = speed
        else:
            max_distance = int(move_force.length())
        distance_moved = 1
        # Get current state
        start = self.rect.copy()
        end = self.rect.copy()
        # Test distance 1 to max
        while distance_moved <= max_distance:
            self.rect = start.copy()
            move_force.scale_to_length(distance_moved)
            self.rect.move_ip(self.round_vector2(move_force))
            if self.collide():
                break
            else:
                end = self.rect.copy()
                distance_moved += 1
        # move to end point
        self.rect = end.copy()
