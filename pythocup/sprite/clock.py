import pygame
from ..model import Side
from datetime import datetime

pygame.font.init()

FONT = pygame.font.Font(None, 40)

class Clock(pygame.sprite.Sprite):

    def __init__(self):
        super().__init__()
        self.reset()

    def update(self):
        time = datetime.fromtimestamp(self.secs)
        text = "{:02d}:{:02d}".format(time.minute,time.second)
        self.image = FONT.render(text, 0, (0,0,0))
        self.rect = self.image.get_rect()
        self.rect.midtop = (400,0)

    def reset(self):
        self.secs = 0

    def set_time(self, secs):
        self.secs = secs
