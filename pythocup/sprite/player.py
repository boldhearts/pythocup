import pygame
from .. import config
from .round_sprite import RoundSprite
from ..model import Side

class Player(RoundSprite):

    def __init__(self, brain, pos):
        pos = pygame.math.Vector2(pos)
        self.brain = brain
        if self.brain.side == Side.LEFT:
            colour = config.LEFT
        else:
            colour =  config.RIGHT
            pos = pygame.math.Vector2(config.SIZE[0] - pos[0], config.SIZE[1] - pos[1])
        super().__init__(30, colour)
        self.rect.center = pos

    def setup(self, ball, goals, self_team, other_team):
        self.brain.setup(goals)
        self.ball = ball
        self.self_team = self_team
        self.other_team = other_team

    def update(self):
        can_kick = self.brain.pre_action(self)
        move, kick = self.brain.action()
        if can_kick:
            self.ball.kick(kick)
        self.move_with_collision(config.PLAYER_SPEED, move)

    def collide(self):
        for player in self.self_team:
            if player != self and pygame.sprite.collide_circle(self, player):
                return True
        for player in self.other_team:
            if pygame.sprite.collide_circle(self, player):
                return True
        return pygame.sprite.collide_circle(self, self.ball.sprite)
