from ..brain import Brain
from pygame.math import Vector2

class FollowBall(Brain):

    def action(self):
        move = self.ball.pos - self.pos
        if self.can_kick:
            return move, self.other_goal.pos - self.ball.pos
        else:
            return move, Vector2(0,0)
