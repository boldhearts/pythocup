from abc import ABC, abstractmethod
from ..model import ObjectType, CupObject, Side
from pygame.math import Vector2

class Brain(ABC):

    def __init__(self, side):
        self.side = side

    def setup(self, goals):
        self.self_goal = CupObject(ObjectType.GOAL, goals.get(self.side).rect.center, self.side)
        other_side = self.side.other()
        self.other_goal = CupObject(ObjectType.GOAL, goals.get(other_side).rect.center, other_side)

    def pre_action(self, body):
        self.ball = CupObject(ObjectType.BALL, body.ball.rect().center)
        self.pos = Vector2(body.rect.center)
        kick_range = body.radius + body.ball.radius() + 5
        self.can_kick = self.pos.distance_to(self.ball.pos) < kick_range

        self.self_team = []
        for player in body.self_team:
            if(player != body):
                self.self_team.append(CupObject(ObjectType.PLAYER, player.rect.center, self.side))

        self.other_team = []
        for player in body.other_team:
            self.other_team.append(CupObject(ObjectType.PLAYER, player.rect.center, self.side.other()))

        return self.can_kick

    def sorted_object(self):
        objects = [self.ball, self.self_goal, self.other_goal] + self.self_team + self.other_team
        objects.sort(key = lambda agent: self.pos.distance_to(agent.pos))
        return objects

    def sided_reflect(self, vec):
        if self.side == Side.RIGHT:
            return vec.rotate(180)
        else:
            return Vector2(vec)

    @abstractmethod
    def action(self):
        raise NotImplementedError('action() has not been setup')
