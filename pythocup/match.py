import pygame
from . import config
from .group import *
from .model import Side
from .sprite import Score
from .brain import FollowBall

pygame.init()
pygame.display.set_caption("Python Cup")
background = pygame.image.load(config.BACKGROUND)
screen = pygame.display.set_mode(config.SIZE)
background = background.convert()

class Match(object):

    def __init__(self):
        super().__init__()
        self.score_board = TeamItem(Score)
        self.clock = SingleClock()
        self.ball = SingleBall(screen.get_rect().center)
        self.goals = TeamGoal()
        self.goals.setup(self.ball, self.score_board)

    def reset(self):
        self.ticks = 0
        self.ball.reset()
        self.score_board.reset()
        self.clock.reset()
        self.done = False
        screen.blit(background, (0, 0))

    def play(self, team_left, team_right):
        self.reset()
        self.team_left = team_left.get_group(Side.LEFT)
        self.team_right = team_right.get_group(Side.RIGHT)
        self.team_left.setup(self.ball,self.goals,self.team_right)
        self.team_right.setup(self.ball,self.goals,self.team_left)
        self.ball.setup(self.team_left, self.team_right)

        clock = pygame.time.Clock()

        while not self.done:
            self.ticks += 1
            clock.tick(config.FPS)
            self.check_events()
            self.clean_screen()
            self.update()
            self.draw()
            pygame.display.flip()


    def check_events(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.done = True
            if event.type is pygame.KEYDOWN and event.key == pygame.K_F12:
                global screen
                if screen.get_flags() & pygame.FULLSCREEN:
                    screen = pygame.display.set_mode(config.SIZE)
                else:
                    screen = pygame.display.set_mode(config.SIZE, pygame.FULLSCREEN)
                screen.blit(background, (0, 0))

    def clean_screen(self):
        self.goals.clear(screen, background)
        self.ball.clear(screen, background)
        self.team_left.clear(screen, background)
        self.team_right.clear(screen, background)
        self.score_board.clear(screen, background)
        self.clock.clear(screen, background)

    def update(self):
        self.goals.update()
        self.ball.update()
        self.team_left.update()
        self.team_right.update()
        self.score_board.update()
        self.clock.set_time(self.get_time())
        self.clock.update()

    def draw(self):
        self.goals.draw(screen)
        self.ball.draw(screen)
        self.team_left.draw(screen)
        self.team_right.draw(screen)
        self.score_board.draw(screen)
        self.clock.draw(screen)

    def get_score(self):
        data = {}
        for score in self.score_board:
            data[score.side] = score.score
        return data

    def get_time(self):
        return self.ticks/config.FPS
