from . import Team, Match, FollowBall

def main():

    empty_team = Team()

    one_player_team = Team()
    one_player_team.put(FollowBall, (100, 300))

    match = Match()
    match.play(one_player_team, empty_team)
    print(match.get_score())

    match.play(empty_team, one_player_team)
    print(match.get_score())

if __name__ == "__main__":
    main()
