# Python Cup Resource Creation

This is a small python script to generate the field and goal images.

## Prerequirements
- Python 3
- Numpy 1.15.4
- Pillow 5.3.0

## Install

You can install the packages with one of the two commands:

- `pip3 install -r requirements.txt`
- `python3 -m pip install -r requirements.txt`

## Rendering

    python3 render_resources.py

# Contribution
You're welcome to contribute. Please have a look at [Gitlab's workflow](https://docs.gitlab.com/ee/workflow/forking_workflow.html) first. It is helpful if each commit addresses a specific feature before requesting a merge. You can achieve this with, e.g., squashing the commits of your working copy.
