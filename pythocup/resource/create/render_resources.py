from PIL import Image, ImageDraw
import numpy as np

GREEN = (34,198,38)
YELLOW = (222, 220, 47)

goal_size = (40,144)
one = np.array([1,1])
field_img_size = np.array([800,500])
size = field_img_size - one

circle_offset = np.array([40,40])
center_UL = np.floor(size / 2)
center_LL = center_UL + np.array([0,1])
center_UR = center_UL + np.array([1,0])
center_LR = center_UL + one

line_offset = np.array([center_UL[0],25])

corner_offset = np.array([20,20])
corner_UL = np.array([40,25])
corner_LR = size - corner_UL
corner_LL = np.array([corner_LR[0] ,corner_UL[1]])
corner_UR = np.array([corner_UL[0] ,corner_LR[1]])

inner_goal_UL = np.array([40,160])
inner_goal_LR = np.array([(inner_goal_UL[0]+80),(size[1]-inner_goal_UL[1])])

outer_goal_UL = np.array([40,90])
outer_goal_LR = np.array([(outer_goal_UL[0]+140),(size[1]-outer_goal_UL[1])])

field_img = Image.new("RGB", tuple(field_img_size), GREEN)

draw = ImageDraw.Draw(field_img)

field_area =  (tuple(corner_UL),tuple(corner_LR))
draw.rectangle(field_area, outline="white", width=2)


center_circle_UL = (tuple(center_UL - circle_offset),tuple(center_UL + circle_offset))
draw.arc(center_circle_UL, 179,271, fill="white", width=2)

center_circle_LL = (tuple(center_LL - circle_offset),tuple(center_LL + circle_offset))
draw.arc(center_circle_LL, 89,181, fill="white", width=2)

center_circle_UR = (tuple(center_UR - circle_offset),tuple(center_UR + circle_offset))
draw.arc(center_circle_UR, 269,1, fill="white", width=2)

center_circle_LR = (tuple(center_LR - circle_offset),tuple(center_LR + circle_offset))
draw.arc(center_circle_LR, 359,91, fill="white", width=2)

left_inner_goal_area = (tuple(inner_goal_UL),tuple(inner_goal_LR))
draw.rectangle(left_inner_goal_area, outline="white", width=2)

right_inner_goal_area = (tuple(size - inner_goal_LR),tuple(size - inner_goal_UL))
draw.rectangle(right_inner_goal_area, outline="white", width=2)

left_outer_goal_area = (tuple(outer_goal_UL),tuple(outer_goal_LR))
draw.rectangle(left_outer_goal_area, outline="white", width=2)

right_outer_goal_area = (tuple(size - outer_goal_LR),tuple(size - outer_goal_UL))
draw.rectangle(right_outer_goal_area, outline="white", width=2)

line_area =  (tuple(line_offset),tuple(size - line_offset))
draw.rectangle(line_area, outline="white", width=2)

corner_UL_area = (tuple(corner_UL - corner_offset),tuple(corner_UL + corner_offset))
draw.arc(corner_UL_area, 0, 90, fill="white", width=2)

corner_LR_area = (tuple(corner_LR - corner_offset),tuple(corner_LR + corner_offset))
draw.arc(corner_LR_area, 180, 270, fill="white", width=2)

corner_UR_area = (tuple(corner_UR - corner_offset),tuple(corner_UR + corner_offset))
draw.arc(corner_UR_area, 270, 0, fill="white", width=2)

corner_LL_area = (tuple(corner_LL - corner_offset),tuple(corner_LL + corner_offset))
draw.arc(corner_LL_area, 90, 180, fill="white", width=2)

field_img.save("../field.png", "PNG")

Image.new("RGB", goal_size , YELLOW).save("../goal.png", "PNG")
