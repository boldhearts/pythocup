from enum import Enum

class ObjectType(Enum):
    BALL = "Ball"
    GOAL = "Goal"
    PLAYER = "Player"
