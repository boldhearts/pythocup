from pygame.math import Vector2
from .side import Side

class CupObject():

    def __init__(self, obj_type, pos, side=None):
        self.type = obj_type
        self.pos = Vector2(pos)
        self.side = side

    def __repr__(self):
        if self.side is not None:
            return self.type.value + " " + self.side.value
        else:
            return self.type.value
