from enum import Enum

class Side(Enum):
    LEFT = 'Left'
    RIGHT = 'Right'

    def other(self):
        if self == Side.LEFT:
            return Side.RIGHT
        else:
            return Side.LEFT
