import os

path = os.path.dirname(__file__)
SIZE = (800,500)

BACKGROUND = os.path.join(path, "resource/field.png")
GOAL = os.path.join(path, "resource/goal.png")

BALL = (100, 100, 100)
LEFT = (200, 10, 10)
RIGHT = (10, 10, 200)

PLAYER_SPEED = 5
BALL_SPEED = 5

FPS = 30
