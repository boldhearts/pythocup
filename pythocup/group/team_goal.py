from .team_item import TeamItem
from ..sprite import Goal

class TeamGoal(TeamItem):

    def __init__(self):
        super().__init__(Goal)

    def setup(self, ball, score_board):
        for goal in self.sprites():
            goal.setup(ball, score_board.get(goal.side.other()))
