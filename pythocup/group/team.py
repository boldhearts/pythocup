import pygame
from .. import config
from ..sprite import Player
from ..brain import Brain

class Team(object):

    def __init__(self):
        super().__init__()
        self.brains = []

    def put(self, BrainClass, pos):
        if not issubclass(BrainClass, Brain):
            raise Exception('The class passed dose not inherit Brain.')
        half = int(config.SIZE[0]/2)
        if pos[0] > half or pos[0] < 0:
            raise Exception('The x value must be between 0 and '+ str(half))
        if pos[1] > config.SIZE[1] or pos[1] < 0:
            raise Exception('The y value must be between 0 and '+ str(config.SIZE[1]))
        self.brains.append({"class": BrainClass, "pos": pos})

    def get_group(self, side):
        return TeamGroup(side, self.brains)

class TeamGroup(pygame.sprite.Group):

    def __init__(self, side, brains):
        super(TeamGroup, self).__init__()
        for brain in brains:
            self.add(Player(brain["class"](side), brain["pos"]))

    def setup(self, ball, goals, other_team):
        for player in self.sprites():
            player.setup(ball, goals, self, other_team)
