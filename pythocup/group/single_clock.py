from pygame.sprite import GroupSingle
from ..sprite import Clock

class SingleClock(GroupSingle):

    def __init__(self):
        super().__init__(Clock())

    def set_time(self, secs):
        self.sprite.set_time(int(secs))

    def reset(self):
        self.sprite.reset()
