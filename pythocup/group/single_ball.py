from pygame.sprite import GroupSingle
from ..sprite import Ball

class SingleBall(GroupSingle):

    def __init__(self, pos):
        super().__init__(Ball(pos))

    def setup(self, *teams):
        self.sprite.setup(*teams)

    def kick(self, vector):
        self.sprite.kick(vector)

    def reset(self):
        self.sprite.reset()

    def rect(self):
        return self.sprite.rect

    def radius(self):
        return self.sprite.radius
