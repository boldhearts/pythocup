import pygame
from ..model import Side

class TeamItem(pygame.sprite.Group):

    def __init__(self, spritetype):
        super().__init__()
        self.left = spritetype(Side.LEFT)
        self.right = spritetype(Side.RIGHT)
        self.add(self.left)
        self.add(self.right)

    def get(self, side):
        if side == Side.LEFT:
            return self.left
        else:
            return self.right

    def reset(self):
        for sprite in self.sprites():
            sprite.reset()
